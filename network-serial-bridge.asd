(defsystem "network-serial-bridge"
  :description "Application that connects a network port to a serial port."
  :author "Eric Timmons <etimmons@mit.edu>"
  :version "0.0.1"
  :licence "MIT"
  :depends-on ("babel"
               "bordeaux-threads"
               "cserial-port"
               "iolib"
               "log4cl"
               (:version "uiop" "3.3.5"))
  :components
  ((:module "src"
    :components
    ((:file "package")
     (:file "defs" :depends-on ("package"))
     (:file "utils" :depends-on ("package"))
     (:file "config" :depends-on ("defs"))
     (:file "bridge" :depends-on ("defs"))
     (:file "channel" :depends-on ("package"))))))

(defsystem "network-serial-bridge/executable"
  :description "An executable for network-serial-bridge"
  :author "Eric Timmons <etimmons@mit.edu>"
  :version "0.0.1"
  :licence "MIT"
  :entry-point "network-serial-bridge/exec::cli-entry"
  :depends-on ("adopt"
               "network-serial-bridge")
  :components
  ((:module "src"
    :components
    ((:file "executable")))))
