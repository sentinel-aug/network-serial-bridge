(require "asdf")

(defmethod asdf:output-files ((op asdf:program-op)
                              (c (eql (asdf:find-system "network-serial-bridge/executable"))))
  (values (list (asdf:system-relative-pathname c "build/network-serial-bridge"))
          t))

(asdf:operate 'asdf:program-op "network-serial-bridge/executable")
