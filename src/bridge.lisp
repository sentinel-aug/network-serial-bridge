(cl:in-package #:network-serial-bridge)

(defclass bridge ()
  ((lock
    :initform (bt:make-lock)
    :reader lock)
   (config
    :initarg :config
    :reader config)
   (channels-ht
    :initform (make-hash-table :test 'equal)
    :reader channels-ht)))

(defmethod port ((bridge bridge))
  (port (config bridge)))

(defmethod hostname ((bridge bridge))
  (hostname (config bridge)))

(defmethod get-channel-config ((bridge bridge) channel-name)
  (get-channel-config (config bridge) channel-name))

(defun start-forwarding-new-channel (bridge channel-info socket buffer start end)
  (bt:with-lock-held ((lock bridge))
    (let ((existing-channel (gethash (name channel-info) (channels-ht bridge))))
      (unless (null existing-channel)
        (stop-channel existing-channel)))
    (setf (gethash (name channel-info) (channels-ht bridge))
          (make-instance 'channel
                          :name (name channel-info)
                          :socket socket
                          :socket-buffer buffer
                          :start start
                          :end end
                          :serial-port-name (serial-port-name channel-info)))))

(defun handle-client-socket (socket bridge)
  (handler-bind ((error (lambda (c)
                          (uiop:print-condition-backtrace c)
                          (invoke-restart 'abort))))
    (setf (sock:socket-option socket :receive-timeout) 1)
    (let ((buffer (make-array 4096 :element-type '(unsigned-byte 8) :initial-element 0))
          (closep t)
          name
          password
          start
          end)
      (unwind-protect
           (progn
             ;; Read the channel name, terminated by a newline
             (multiple-value-bind (line new-start new-end) (read-line-from-socket socket buffer nil nil)
               (setf name (uiop:stripln line)
                     start new-start
                     end new-end))
             (when (>= start end)
               (setf start nil
                     end nil))
             (log:info "Read ~S, waiting for password" name)
             ;; Read the password, terminated by a newline.
             (multiple-value-bind (line new-start new-end) (read-line-from-socket socket buffer start end)
               (setf password (uiop:stripln line)
                     start new-start
                     end new-end))
             (when (>= start end)
               (setf start nil
                     end nil))
             (log:info "Read password")
             ;; Now we can look up the channel info to see if the channel name and password match.
             (let ((channel-config (get-channel-config bridge name)))
               (when (null channel-config)
                 (error "no such channel"))
               (unless (equal password (password channel-config))
                 (error "password mismatch"))
               ;; We're golden, start bridging this channel!  First, determine if
               ;; there's already a bridge running. If so, destroy it.
               (setf closep nil)
               (start-forwarding-new-channel bridge channel-config socket buffer start end)))
        (when closep
          (close socket))))))

(defun start (bridge)
  (log:info "starting bridge on port ~D" (port bridge))
  (unwind-protect
       (sock:with-open-socket (server :connect :passive
                                      :address-family :internet
                                      :type :stream
                                      :reuse-address t)
         (sock:bind-address server (hostname bridge) :port (port bridge))
         (sock:listen-on server :backlog 5)
         (loop
           (let ((client (iolib/sockets:accept-connection server)))
             (bt:make-thread (lambda () (handle-client-socket client bridge))))))
    (maphash (lambda (k v)
               (declare (ignore k))
               (stop-channel v))
             (channels-ht bridge))))
