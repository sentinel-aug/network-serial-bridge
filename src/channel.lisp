(cl:in-package #:network-serial-bridge)

(defclass channel ()
  ((name
    :initarg :name
    :reader name)
   (lock
    :initform (bt:make-lock)
    :reader lock)
   (stop-p
    :initform nil
    :accessor stop-p)
   (socket
    :initarg :socket
    :reader socket)
   (serial-port
    :reader serial-port)
   (socket-thread
    :reader socket-thread)
   (serial-thread
    :reader serial-thread)))

(defun pump-serial-port (channel)
  (log:info "Starting to pump serial port for ~A" (name channel))
  (unwind-protect
       (let ((buffer (make-array 4096 :element-type '(unsigned-byte 8) :initial-element 0))
             end)
         (loop
           (when (stop-p channel)
             (return))
           (handler-case
               (setf end (serial:read-serial-byte-vector buffer (serial-port channel) :timeout-ms 500))
             (serial:timeout-error ()
               (setf end 0)))
           (unless (zerop end)
             (log:debug "read data from serial port for ~A" (name channel))
             (ignore-errors
              (let ((string (babel:octets-to-string buffer :end end)))
                (log:debug "read:~%~S" string)))
             (sock:send-to (socket channel) buffer :end end))))
    (stop-channel channel)))

(defun pump-socket (channel)
  (log:info "Starting to pump socket for ~A" (name channel))
  (unwind-protect
       (let ((buffer (make-array 4096 :element-type '(unsigned-byte 8) :initial-element 0))
             end)
         (loop
           (when (stop-p channel)
             (return))
           (handler-case
               (progn
                 (setf end (safe-read-from-socket (socket channel) buffer)))
             (iomux:poll-timeout ()
               (setf end 0))
             (end-of-file ()
               (log:info "Channel ~A closed by sender" (name channel))
               (return)))
           (unless (zerop end)
             (log:debug "read data from socket for ~A" (name channel))
             (ignore-errors
              (let ((string (babel:octets-to-string buffer :end end)))
                (log:debug "read:~%~S" string)))
             (serial:write-serial-byte-vector buffer (serial-port channel) :end end))))
    (stop-channel channel)))

(defun stop-channel (channel)
  (let ((continue-shutdown-p nil))
    (bt:with-lock-held ((lock channel))
      (unless (stop-p channel)
        (setf continue-shutdown-p t)
        (setf (stop-p channel) t)))
    (when continue-shutdown-p
      (setf (stop-p channel) t)
      (unless (eq (bt:current-thread) (serial-thread channel))
        (bt:join-thread (serial-thread channel)))
      (unless (eq (bt:current-thread) (socket-thread channel))
        (bt:join-thread (socket-thread channel)))
      (ignore-errors (close (socket channel)))
      (ignore-errors (serial:close-serial (serial-port channel)))
      (log:info "Channel closed for ~A" (name channel)))))

(defmethod initialize-instance :after ((channel channel) &key serial-port-name start end socket-buffer)
  (let ((serial-port (serial:open-serial serial-port-name :baud-rate 115200)))
    (serial:set-serial-state serial-port :dtr t :rts t)
    (setf (slot-value channel 'serial-port) serial-port)
    (unless (null start)
      (serial:write-serial-byte-vector socket-buffer serial-port :start start :end end))
    (setf (slot-value channel 'serial-thread)
          (bt:make-thread (lambda () (pump-serial-port channel))))
    (setf (slot-value channel 'socket-thread)
          (bt:make-thread (lambda () (pump-socket channel))))))
