(cl:in-package #:network-serial-bridge)

(defclass channel-config ()
  ((name
    :initarg :name
    :reader name)
   (password
    :initarg :password
    :reader password)
   (serial-port-name
    :initarg :serial-port-name
    :reader serial-port-name)))

(defclass config ()
  ((lock
    :initform (bt:make-lock)
    :reader lock)
   (channels-ht
    :initform (make-hash-table :test 'equal)
    :reader channels-ht)
   (hostname
    :initform sock:+ipv4-unspecified+
    :reader hostname)
   (port
    :initform 9045
    :initarg :port
    :accessor port)))

(defmethod initialize-instance :after ((config config) &key channels)
  (loop :for channel :in channels
        :do (setf (gethash (name channel) (channels-ht config)) channel)))

(defmethod get-channel-config ((config config) channel-name)
  (bt:with-lock-held ((lock config))
    (gethash channel-name (channels-ht config))))

(defun read-config-from-file (path)
  (let ((config-sexp (uiop:read-file-form path)))
    (apply #'make-instance 'config
           :channels (mapcar (lambda (args) (apply #'make-instance 'channel-config args))
                             (getf config-sexp :channels))
           (uiop:remove-plist-key :channels config-sexp))))
