(uiop:define-package #:network-serial-bridge/exec
  (:use #:cl)
  (:local-nicknames (#:bridge #:network-serial-bridge)))

(cl:in-package #:network-serial-bridge/exec)

(defparameter *option-help*
  (adopt:make-option
   :help
   :long "help"
   :help "Show help and exit"
   :reduce (constantly t)))

(defparameter *option-port*
  (adopt:make-option
   :port
   :parameter "PORT"
   :long "port"
   :help "Set network port"
   :reduce (lambda (prev new)
             (declare (ignore prev))
             (parse-integer new))))

(defparameter *option-config*
  (adopt:make-option
   :config-file
   :parameter "CONFIG-FILE"
   :short #\c
   :help "Set path to configuration file"
   :initial-value "/etc/network-serial-bridge/network-serial-bridge.conf"
   :reduce #'adopt:last))

(defparameter *ui*
  (adopt:make-interface
   :name "network-serial-bridge"
   :summary "Run a server that bridges network connections to local serial ports"
   :usage "[OPTIONS]"
   :help "Run a server that bridges network connections to local serial ports"
   :contents (list *option-help*
                   *option-port*
                   *option-config*)))

(defun cli-entry ()
  (setf *debugger-hook* (lambda (condition old-hook)
                          (declare (ignore old-hook))
                          (uiop:print-condition-backtrace condition)
                          (uiop:quit 1)))
  (multiple-value-bind (args options)
      (adopt:parse-options *ui*)
    (declare (ignore args))
    (when (gethash :help options)
      (adopt:print-help *ui*)
      (uiop:quit))
    (log:config :debug)
    (let ((config (bridge::read-config-from-file (gethash :config-file options))))
      (unless (null (gethash :port options))
        (setf (bridge::port config) (gethash :port options)))
      (bridge::start (make-instance 'bridge::bridge :config config)))))
