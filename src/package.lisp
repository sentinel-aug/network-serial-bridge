(uiop:define-package #:network-serial-bridge
  (:use #:cl)
  (:local-nicknames (#:serial #:cserial-port)
                    (#:sock #:iolib/sockets)))
