(cl:in-package #:network-serial-bridge)

(defun safe-read-from-socket (socket buffer)
  (iomux:wait-until-fd-ready (sock:socket-os-fd socket) :input 1 t)
  (let ((count (nth-value 1 (sock:receive-from socket :buffer buffer))))
    (when (zerop count)
      (error 'end-of-file :stream socket))
    count))

(defun read-line-or-partial-from-buffer (buffer start end)
  "Read a line or partial line from the BUFFER as a string. Returns three
VALUES, the string, T if there was a newline, and the index of any remaining
characters."
  (let ((newline-pos (position (char-code #\Newline) buffer :start start :end end)))
    (values (babel:octets-to-string buffer :start start :end newline-pos)
            (not (null newline-pos))
            (unless (null newline-pos)
              (1+ newline-pos)))))

(defun read-line-from-socket (socket buffer start end)
  (let ((out ""))
    (loop
      (when (null start)
        (iomux:wait-until-fd-ready (sock:socket-os-fd socket) :input)
        (setf start 0
              end (nth-value 1 (sock:receive-from socket :buffer buffer))))
      (multiple-value-bind (string newline-p next-index)
          (read-line-or-partial-from-buffer buffer start end)
        (setf out (concatenate 'string out string)
              start nil)
        (when (> (length out) 1024)
          (error "too long of a name"))
        (when newline-p
          (return (values out next-index end)))))))
